import numpy as np
import os
feacture = open("/media/smie/Disk2/fashion_fun/recommend/deep_feacture/0/feacture.txt")
center_feacture = open("/media/smie/Disk2/fashion_fun/recommend/cluster/test_kmeans/center_feacture.txt")


center_root = "/media/smie/Disk2/fashion_fun/recommend/cluster/test_kmeans/result/{}/center.txt"
cluster_root = "/media/smie/Disk2/fashion_fun/recommend/cluster/test_kmeans/result/{}/{}.txt"

#read initial center from disk
center = []
for line in center_feacture:
	temp = line.split(" ")
	attr = []
	for i in range(0,2048):
		attr.append(float(temp[i]))
	center.append(attr)

new_center = np.array(center)
center = np.zeros((50,2048))
times = 0

while((times < 60)):
	#initial parameter 
	os.mkdir("/media/smie/Disk2/fashion_fun/recommend/cluster/test_kmeans/result/{}/".format(times))
	center = new_center
	new_center = np.zeros((50,2048))
	cluster = [[] for x in range(50)]
	id = 1
	feacture = open("/media/smie/Disk2/fashion_fun/recommend/deep_feacture/0/feacture.txt")
	for line in feacture:
		#transfer input into float
		temp = line.split(" ")
		attr = []
		for i in range(0,2048):
			attr.append(float(temp[i]))
		attr = np.array(attr)
		#compute distance and find min
		min_dis = 10000000
		category = -1
		#find the cluster of id		
		for j in range(0,50):
			distance = np.dot(attr,attr) + np.dot(center[j],center[j]) - 2 * np.dot(attr,center[j])
			if(distance < min_dis):
				min_dis = distance
				category = j
		#record id into its category
		cluster[category].append(id)
		new_center[category] += attr
		#print category,id
		id += 1

	#computer_new_center
	sum = 0
	for i in range(0,50):
		if(len(cluster[i]) > 0):
			sum += len(cluster[i]) 
			new_center[i] /= len(cluster[i]) 
	print(sum)
		
	#record center
	f_r = open(center_root.format(times),"w")
	for i in range(0,50):
		for j in new_center[i]:
			f_r.write(str(j) + " ")
		f_r.write("\n")

	#record culster:
	for i in range(0,50):
		f_r = open(cluster_root.format(times,i),"w")
		for j in cluster[i]:
			f_r.write(str(j) + "\n")
	print times
	times += 1

print("finished, times = ", times)
#record cluster	
