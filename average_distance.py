import numpy as np
import operator
result_root = "/Users/RuoguLin/Desktop/59/{}.txt"

re ={}
for i in range(0,200):
    file = open(result_root.format(i))
    dis = []
    for line in file:
        temp = line.split(" ")
        dis.append(float(temp[1]))
    dis = np.array(dis)
    re[i] = np.mean(dis)

sorted_re = sorted(re.iteritems(), key=operator.itemgetter(1))

for ele in sorted_re:
    if ele[1] < 300:
        print ele[0],ele[1]