import numpy as np
import time

#center_root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/result_260/{}/center.txt"
cluster_id = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/classify/test_data/test_data/{}.txt"
res = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/classify/result1/{}.txt"
root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/classify/inner_dis.txt"

#read initial center from disk
center = []
for center_id in range(0,260):
	center_feacture = open("/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/classify/center/center{}.txt".format(center_id))
	for line in center_feacture:
		temp = line.split(" ")
		attr = []
		for i in range(0,2048):
			attr.append(float(temp[i]))
		center.append(attr)

center = np.array(center) #2600 * 4096

file = open(root,"w")
for _id in range(0,260):
	for j in range(_id * 10, _id * 10 + 10):
		for k in range(_id * 10, _id * 10 + 10):
			temp_dis = np.dot(center[j],center[j]) + np.dot(center[k],center[k]) - 2 * np.dot(center[j],center[k])
			file.write(str(temp_dis) + "\n")











