import matplotlib.pyplot as plt
file = open("/Users/RuoguLin/Desktop/log.txt")

accuracy = []
loss = []
num = 0
flag = True
for line in file:
    if "accuracy = " in line:
        temp = line.split(" ")
        accuracy.append(float(temp[len(temp) - 1]))
    if "loss = " in line and "Iteration" in line:
        num += 1
        if num == 50 or flag:
            temp = line.split(" ")
            loss.append(float(temp[len(temp) - 1]))
            num = 0
            flag = False



x1 = range(0,51000,1000)

plt.xlabel("Iteration Times")
plt.ylabel("loss")
plt.plot(x1,loss)
plt.show(loss)
