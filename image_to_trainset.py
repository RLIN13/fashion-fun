import os

image_root = "D:\Vogue Clusters\Final\image\{}"
train = open("D:\Vogue Clusters\Final Result/train.txt","w")
data = open("D:\Vogue Clusters\Final Result/data.txt","w")
val = open("D:\Vogue Clusters\Final Result/val.txt","w")
label_map = open("D:\Vogue Clusters\Final Result\label_map.txt","w")
label_map.write("label original_id train_number val_number" + "\n")

class_id = 0
v_num = 0
for i in range(0,108):

	for root, dirs, files in os.walk(image_root.format(i)):

		if len(files) > 500:
			train_num = int(len(files) * 0.7) + 1
			val_num = len(files) - train_num
			num = 0
			for file in files:
				name = file.split("_")[1]
				data.write(name + " " + str(class_id) + "\n")
				if(num < train_num):
					train.write(name + " " + str(class_id) + "\n")
				else:
					val.write(name + " " + str(class_id) + "\n")
				num += 1

			label_map.write(str(class_id) + " " + str(i) + " " + str(train_num) + " " + str(val_num) + "\n")
			class_id += 1


