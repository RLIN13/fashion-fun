import os
import random

image_root = "D:\Vogue Clusters/final_results\{}"
re = open("D:\Vogue Clusters/final_results/sed.txt","w")

for i in range(0,197):
    for root,dirs, files in os.walk(image_root.format(i)):
        if len(files) > 20:
            v = []
            for file in files:
                if "v" in file:
                    v.append(file)
            for ele in v:
                files.remove(ele)
            random.shuffle(files)
            for j in range(0,10):
                re.write(files[j].split("_")[1] + " " + str(i) + "\n")
