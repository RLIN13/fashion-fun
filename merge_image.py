from PIL import Image
import os
import random

def mergei(files, output_file):
    tot = len(files)
    img = Image.open(files[0])
    region = (63, 0, 195, 255)
    img  = img.crop(region)
    w, h = img.size[0], img.size[1]
    merge_img = Image.new('RGB', (w * tot, h), 0xffffff)
    i = 0
    for f in files:
        print(f)
        img = Image.open(f)
        #img = img.resize((w,h))
        region = (63, 0, 195, 255)
        img = img.crop(region)
        merge_img.paste(img, (i, 0))
        i += w
    merge_img.save(output_file)

def mergej(files, output_file):
    tot = len(files)
    img = Image.open(files[0])
    w, h = img.size[0], img.size[1]
    merge_img = Image.new('RGB', (w, h * tot), 0xffffff)
    j = 0
    for f in files:
        print(f)
        img = Image.open(f)
        merge_img.paste(img, (0, j))
        j += h
    merge_img.save(output_file)

image_root = "/Users/RuoguLin/Desktop/re_fashion/recommendation/1/"

input = []
output = "/Users/RuoguLin/Desktop/re_fashion/recommendation/1/6.jpg"
'''
for root, dirs, files in os.walk(image_root):
    random.shuffle(files)
    for i in range(0,7):
        input.append(image_root + files[i])

mergei(input,output)
'''
for i in range(1,6):
    input.append(image_root + str(i) + ".jpg")

mergej(input,output)
