import numpy as np
file = open("/Users/RuoguLin/Desktop/59/center.txt")
valid = [78,125,3,140,1]
num = 0
map = {}
map[0] = 1
map[1] = 3
map[2] = 78
map[3] = 125
map[4] = 140
feature_kt = []
for line in file:
    if num in valid:
        attr = []
        temp = line.split(" ")
        for i in range(0,2048):
            attr.append(float(temp[i]))
        feature_kt.append(attr)
    num += 1

feature_kt = np.array(feature_kt)

fei = [154,221,244]
feature_sed = []
file = open("/Users/RuoguLin/Desktop/feature/feature_top10.txt")
cluster_id = 0
sum = 0
num = 0
for line in file:
    num += 1
    attr = []
    temp = line.split(" ")
    for i in range(0,2048):
        attr.append(float(temp[i]))
    attr = np.array(attr)
    sum += attr
    if num == 10:
        mean = sum / 10
        num = 0
        sum = 0
        feature_sed.append(mean)
        cluster_id += 1
    if num == 9:
        if cluster_id in fei:
            mean = sum / 9
            sum = 0
            num = 0
            feature_sed.append(mean)
            cluster_id += 1


feature_sed = np.array(feature_sed)
target = [4, 17, 31, 33, 35, 43, 46, 47, 50, 52, 59, 66, 67, 71, 78, 86, 89, 91, 92, 94, 97, 99, 100, 101, 114, 119, 124, 132, 138, 140, 146, 153, 160, 161, 162, 167, 178, 179, 183, 195, 208, 210, 217, 218, 220, 221, 223, 232, 235, 245, 259]


for i in range(0,5):
    min_dis = 100000
    cate = 0
    for j in target:
        temp_dis = np.dot(feature_kt[i],feature_kt[i]) + np.dot(feature_sed[j],feature_sed[j]) - 2 * np.dot(feature_kt[i],feature_sed[j])
        if temp_dis < min_dis:
            min_dis = temp_dis
            cate = j
    print map[i],cate,min_dis