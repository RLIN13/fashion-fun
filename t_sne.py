import numpy as np
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


center_root = "/Users/RuoguLin/Desktop/fashion/99-1/center.txt"
f = open(center_root)

center = np.zeros([260,2048])

x = 0
for line in f:
	temp = line.split(" ")
	for y in range(0,2048):
		center[x,y] = float(temp[y])
	x += 1

model = TSNE(n_components=3, random_state=0)
np.set_printoptions(suppress=True)
newcenter = model.fit_transform(center)

print newcenter
#plt.plot(newcenter[:,0],newcenter[:,1],'ro') 
ax=plt.subplot(111,projection='3d') 
ax.scatter(newcenter[:,0],newcenter[:,1],newcenter[:,2])
plt.show()