import numpy as np
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

feacture_root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/feacture.txt"
center_root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/result_260/99/center.txt"
cluster_root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/result_260/99/{}.txt"
rank_root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/result_260/99/rank{}.txt"

for i in range(0,260):
	print i	
	cluster_file = open(cluster_root.format(i))
	cluster = []
	d_id = {}
	# read the data for cluster
	for line in cluster_file:
		cluster.append(int(line))
	cluster.sort()
	id = 1
	now_id = cluster.pop(0)
	feacture =  open(feacture_root)
	cluster_feacture = []
	for line in feacture:
		if id == now_id:
			temp = line.split(" ")
			attr = []
			for j in range(0,2048):
				attr.append(float(temp[j]))
			attr = np.array(attr)
			cluster_feacture.append(attr)
			if len(cluster) > 0:
				now_id = cluster.pop(0)
		id += 1

	if len(cluster) != 0:
		print ("error")
		print len(cluster)
		break
	cluster_feacture = np.array(cluster_feacture)
	model = TSNE(n_components=2, random_state=0)
	np.set_printoptions(suppress=True)
	newcenter = model.fit_transform(cluster_feacture)
	plt.figure(0)
	#plt.plot(newcenter[:,0],newcenter[:,1],'ro') 
	ax=plt.subplot(111,projection='3d') 
	ax.scatter(newcenter[:,0],newcenter[:,1])
	plt.savefig("/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/firgue/" + str(i) + ".jpg")




