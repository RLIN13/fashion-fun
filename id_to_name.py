train_num = [9457, 8976, 9241, 9005, 8712, 13378, 14760, 16553, 20297, 19054, 21297, 29104, 33368, 38716, 37117, 30459, 25967]
val_num = [1900, 2559, 2409, 2146, 2615, 2645, 4185, 4181, 5284, 5488, 5061, 6562, 7863, 8881, 9889, 7382, 6679]

def get_name(id,array):
	sum = 0
	num = 0
	for i in array:
		if id <= i:
			name = chr(65 + num) + str(id) + ".jpg"
			category = chr(65 + num)
			return category, name
		else:
			num += 1
			id -= i

def name_to_id(name):
    #spilt
    flag = name.split("_")[0]
    cate = ord(name.split("_")[1][0])-64
    id = int(name.split("_")[1].split(".")[0][1:])
    temp = []
    if flag == "t":
        temp = train_num
    else:
        temp = val_num
    re = 0
    for i in range(0,cate):
        re += temp[i]
    return re + id


print get_name(name_to_id("t_A6225.jpg"),train_num)

