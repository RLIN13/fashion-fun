import os
url = "D:/Vogue Clusters/final_results"

dict = {}
res = {}

for root, dirs, files in os.walk(url):
    for f in files:
        clas = os.path.split(root)[1]
        if not dict.has_key(f):
            arr = []
            arr.append(clas)
            dict[f] = arr
        else:
            dict[f].append(clas)

for i in dict:
    if len(dict[i])!= 1:
        res[i] = dict[i]

arr_res = res.values()
pair = {}

for ary in arr_res:
    for j in range(0,len(ary)):
        k = j+1
        while k < len(ary):
            a = []
            a.append(ary[j])
            a.append(ary[k])
            a.sort()
            k += 1
            a = ",".join(a)
            if not pair.has_key(a):
                pair[a] = 1
            else:
                pair[a] += 1

f_url = "D:/Vogue Clusters/duplicate.txt"
f = open(f_url,"w")

for key in pair:
    f.write(key+":"+str(pair[key])+"\n")
