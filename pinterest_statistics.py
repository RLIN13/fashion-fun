#coding=utf-8
import os

set = set()
cnt_single_album_user = 0
cnt_over_3_album_user = 0
cnt_over_5_album_user = 0
cnt_over_10_album_user = 0
cnt_over_500_img_user = 0
cnt_over_500_img_user_num = 0
cnt_over_1000_img_user = 0
cnt_over_1000_img_user_num = 0
cnt_over_2000_img_user = 0
cnt_over_2000_img_user_num = 0
cnt_over_3000_img_user = 0
cnt_over_3000_img_user_num = 0
cnt_over_5000_img_user = 0
cnt_over_5000_img_user_num = 0
files_num = []
total_img = 0

for root, dirs, files in os.walk('D:\Pinterest Image\selected_user_images'):

    if len(dirs) == 1:
        cnt_single_album_user += 1

    if len(dirs) >= 3:
        cnt_over_3_album_user +=1

    if len(dirs) >= 5:
        cnt_over_5_album_user +=1

    if len(dirs) >= 10:
        cnt_over_10_album_user +=1

    if len(files) >= 500:
        cnt_over_500_img_user += 1
        cnt_over_500_img_user_num += len(files)

    if len(files) >= 1000:
        cnt_over_1000_img_user += 1
        cnt_over_1000_img_user_num += len(files)

    if len(files) >= 2000:
        cnt_over_2000_img_user += 1
        cnt_over_2000_img_user_num += len(files)

    if len(files) >= 3000:
        cnt_over_3000_img_user += 1
        cnt_over_3000_img_user_num += len(files)

    if len(files) >= 5000:
        cnt_over_5000_img_user += 1
        cnt_over_5000_img_user_num += len(files)

    files_num.append(len(files))

for i in files_num:
    if i == 0:
        files_num.remove(0)

files_num.sort()

for j in files_num:
    total_img += j

print "The maximum images number of an user is:", max(files_num)
print "The median images number of an user is:", files_num[len(files_num)/2]
print "The average images number of an user is:", total_img/len(files_num)
print "Users having 1 pinboards:", cnt_single_album_user
print "Users having over 3 pinboards:", cnt_over_3_album_user
print "Users having over 5 pinboards:", cnt_over_5_album_user
print "Users having over 10 pinboards:", cnt_over_10_album_user
print "Users having over 500 images:", cnt_over_500_img_user, "number of these images in total:", cnt_over_500_img_user_num
print "Users having over 1000 images:", cnt_over_1000_img_user, "number of these images in total:", cnt_over_1000_img_user_num
print "Users having over 2000 images:", cnt_over_2000_img_user, "number of these images in total:", cnt_over_2000_img_user_num
print "Users having over 3000 images:", cnt_over_3000_img_user, "number of these images in total:", cnt_over_3000_img_user_num
print "Users having over 5000 images:", cnt_over_5000_img_user, "number of these images in total:",cnt_over_5000_img_user_num