import numpy as np
res_root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/classify/center/new_center.txt"
center_root = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/classify/center/center{}.txt"
result = "/media/smie/Disk2/fashion_fun/deep-residual-networks-master/K-MEAN/classify/center/dis.txt"

r_file = open(result,"w")

# read new center
center = []
file = open(res_root)
for line in file:
	temp = line.split(" ")
	attr = []
	for j in range(0,2048):
		attr.append(float(temp[j]))
	center.append(attr)

center = np.array(center)
	



for i in range(0,260):
	#print root.format(i)
	file = open(center_root.format(i))
	cluster_feacture = []
	sum = np.zeros(2048)
	for line in file:
		attr = []
		temp = line.split(" ")
		for j in range(0,2048):
			attr.append(float(temp[j]))
		attr = np.array(attr)
		sum += attr
		cluster_feacture.append(attr)
	file.close()
	cluster_feacture = np.array(cluster_feacture)
	# all date for one cluster are stored in cluster_feacture:
	distance = []
	for j in range(0,10):
		temp = np.dot(center[i],center[i]) + np.dot(cluster_feacture[j],cluster_feacture[j]) - 2 * np.dot(center[i],cluster_feacture[j])
		distance.append(temp)
	distance.sort
	for di_e in distance:
		r_file.write(str(di_e) + " ")
	r_file.write("\n")