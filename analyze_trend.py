import os
import matplotlib.pyplot as plt
image_root = "/Users/YuxiaoChen/Desktop/fashion/final_result/part1/image without outliers (manual)/image/{}"
year_num = [11357,11535,11650
,11151
,11327
,16023
,18945
,20734
,25581
,24542
,26358
,35666
,41231
,47597
,47006
,37841
,32646
]

for i in range(0,260):
    re = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    for root,dirs,files in os.walk(image_root.format(i)):
        if len(files) > 500:
            for file in files:
                if ".jpg" in file:
                    year = ord(file[2]) - 65
                    re[year] += 1
            for j in range(0,17):
                re[j] /= float(year_num[j])
            x = range(2000,2017)
            plt.xlabel("Year")
            plt.ylabel("proportion")
            plt.plot(x,re)
            plt.savefig("/Users/YuxiaoChen/Desktop/re_fashion/plot/" + str(i) + ".jpg")
            plt.close('all')



